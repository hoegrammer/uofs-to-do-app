from typing import Dict


class ToDoItemCreate:
    def __init__(self, dikt: Dict):
        self.details = dikt['details']
        self.title = dikt['title']

