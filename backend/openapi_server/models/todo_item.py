from typing import Dict

from openapi_server.database import db


class TodoItem(db.Model):
    """
    Defines the data
    """
    id = db.Column(db.Integer(), primary_key=True)
    created_at = db.Column(db.DateTime())
    updated_at = db.Column(db.DateTime())
    title = db.Column(db.String())
    details = db.Column(db.String())
    __table_args__ = {'sqlite_autoincrement': True}

    def serialise(self):
        return {"id": self.id,
                "created_at": self.created_at,
                "updated_at": self.updated_at,
                "title": self.title,
                "details": self.details}

