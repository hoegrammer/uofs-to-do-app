from typing import Dict


class ToDoItemUpdate:
    def __init__(self, dikt: Dict):
        self.details = dikt['details']
        self.title = dikt['title']