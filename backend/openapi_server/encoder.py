from connexion.apps.flask_app import FlaskJSONEncoder

from openapi_server.models.todo_item import TodoItem


class JSONEncoder(FlaskJSONEncoder):
    include_nulls = False

    def default(self, o):
        """
        TodoItem is an SQLAlchemy model and these are not serializable by the default JSON encoder that comes with Flask.
        This implements a custom encoder which calls the serialise method on TodoItem but otherwise uses the default encoder.
        """
        if isinstance(o, TodoItem):
            return o.serialise()
        return FlaskJSONEncoder.default(self, o)
