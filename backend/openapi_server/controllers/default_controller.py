import datetime
from typing import List, Optional
from typing import Tuple

import connexion
from openapi_server.database import db

from openapi_server.models.todo_item import TodoItem
from openapi_server.models.todo_item_create import ToDoItemCreate
from openapi_server.models.todo_item_update import ToDoItemUpdate


def create_item_todo_items__post() -> Tuple[object, int]:
    todo_item_create = ToDoItemCreate(connexion.request.get_json())
    todo_item = TodoItem(created_at=datetime.datetime.now(), updated_at=datetime.datetime.now(),
                         title=todo_item_create.title, details=todo_item_create.details)
    db.session.add(todo_item)
    db.session.commit()
    return todo_item, 200


def delete_item_todo_items__item_id__delete(item_id: int) -> Tuple[object, int]:
    item = db.session.get(TodoItem, item_id)
    if item is None:
        return "Item not found", 404
    db.session.delete(item)
    db.session.commit()
    return 'Deleted', 200


def read_item_todo_items__item_id__get(item_id: int) -> Tuple[object, int]:
    return db.get_or_404(TodoItem, item_id)


def read_items_todo_items__get(skip: Optional[int] = None, limit: Optional[int] = None) -> Tuple[List[TodoItem], int]:
    return TodoItem.query.all(), 200


def read_root__get() -> str:
    return 'do some magic!'


def update_item_todo_items__item_id__patch(item_id: int) -> Tuple[object, int]:  # noqa: E501
    if connexion.request.is_json:
        todo_item_update = ToDoItemUpdate(connexion.request.get_json())  # noqa: E501
        todo_item: TodoItem = db.session.get(TodoItem, item_id)
        todo_item.title = todo_item_update.title
        todo_item.details = todo_item_update.details
        todo_item.updated_at = datetime.datetime.now()
        db.session.commit()
        return todo_item, 200
