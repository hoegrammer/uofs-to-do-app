#!/usr/bin/env python3

import connexion

from openapi_server import encoder
from flask_cors import CORS
from openapi_server.database import db


def main():
    app = connexion.App(__name__, specification_dir='./openapi/')
    app.app.json_encoder = encoder.JSONEncoder
    app.app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///todoitems.db"
    db.init_app(app.app)
    app.add_api('openapi.yaml',
                arguments={'title': 'API'},
                pythonic_params=True)
    CORS(app.app)
    with app.app.app_context():
        db.create_all()
    app.run(port=8000)


if __name__ == '__main__':
    main()
