import unittest
from datetime import datetime
from typing import List, Dict

from flask import json

from openapi_server.database import db
from openapi_server.models.todo_item import TodoItem
from openapi_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    SQLALCHEMY_DATABASE_URI = "sqlite://testing.db"
    TESTING = True

    def test_create_item_todo_items_post(self):
        # Send a post request to add an item
        todo_item_create = {"details": "foo", "title": "bar"}
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/todo_items/',
            method='POST',
            headers=headers,
            data=json.dumps(todo_item_create),
            content_type='application/json')
        self.assert200(response)

        # Get the newly created item id from the response
        data = json.loads(response.data)
        item_id: int = data['id']

        # Get the item from the DB and check that the field values have been set
        item = db.session.get(TodoItem, item_id)
        self.assertEqual('foo', item.details)
        self.assertEqual('bar', item.title)

    def test_delete_item_todo_items_item_id_delete(self):
        # Put an item in the DB
        item = TodoItem(
            created_at=datetime.now(),
            updated_at=datetime.now(),
            title='foo',
            details='bar'
        )
        db.session.add(item)
        db.session.commit()

        # refresh to get the id, in order to delete it
        db.session.refresh(item)

        # delete and check for successful response
        headers = {
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/todo_items/{item_id}'.format(item_id=item.id),
            method='DELETE',
            headers=headers)
        self.assert200(response)
        self.assertEqual(json.loads(response.data), 'Deleted')

        # check that the item no longer exists
        self.assertIsNone(db.session.get(TodoItem, item.id))

    def test_delete_item_todo_items_item_id_delete_404(self):
        # try to delete a nonexistent item, check for 404
        headers = {
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/todo_items/{item_id}'.format(item_id=999),
            method='DELETE',
            headers=headers)
        self.assert404(response)

    def test_read_item_todo_items_item_id_get(self):
        # Put an item in the DB
        item = TodoItem(
            created_at=datetime.now(),
            updated_at=datetime.now(),
            title='some title',
            details='some details'
        )
        db.session.add(item)
        db.session.commit()

        # refresh to get the id
        db.session.refresh(item)

        # test that the get function retrieves it
        headers = {
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/todo_items/{item_id}'.format(item_id=item.id),
            method='GET',
            headers=headers)
        self.assert200(response)
        data = json.loads(response.data)
        self.assertEqual('some title', data['title'])
        self.assertEqual('some details', data['details'])

    def test_delete_item_todo_items_item_id_get_404(self):
        # try to get a nonexistent item, check for 404
        headers = {
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/todo_items/{item_id}'.format(item_id=999),
            method='GET',
            headers=headers)
        self.assert404(response)

    def test_read_items_todo_items_get(self):
        # Put an item in the DB
        item = TodoItem(
            created_at=datetime.now(),
            updated_at=datetime.now(),
            title='another title',
            details='some more details'
        )
        db.session.add(item)
        db.session.commit()

        # refresh to get the id
        db.session.refresh(item)

        # get all items should return an array containing that item
        query_string = [('skip', 0),
                        ('limit', 20)]
        headers = {
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/todo_items/',
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response)
        items: List[Dict] = json.loads(response.data)
        self.assertGreater(len(items), 0)
        self.assertEqual('another title', items[0]['title'])
        self.assertEqual('some more details', items[0]['details'])

    def test_update_item_todo_items_item_id_patch(self):
        # Put an item in the DB
        item = TodoItem(
            created_at=datetime.now(),
            updated_at=datetime.now(),
            title='another title',
            details='some more details'
        )
        db.session.add(item)
        db.session.commit()

        # refresh to get the id
        db.session.refresh(item)

        # update it
        todo_item_update = {"title": "foo", "details": "bar"}
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/todo_items/{item_id}'.format(item_id=item.id),
            method='PATCH',
            headers=headers,
            data=json.dumps(todo_item_update),
            content_type='application/json')
        self.assert200(response)

        # retrieve it and check it has been updated
        item = db.session.get(TodoItem, item.id)
        self.assertEqual('foo', item.title)
        self.assertEqual('bar', item.details)


if __name__ == '__main__':
    unittest.main()
