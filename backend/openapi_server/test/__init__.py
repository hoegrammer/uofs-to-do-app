import logging

import connexion
from flask_testing import TestCase

from openapi_server.database import db
from openapi_server.encoder import JSONEncoder


class BaseTestCase(TestCase):

    def create_app(self):
        logging.getLogger('connexion.operation').setLevel('ERROR')
        app = connexion.App(__name__, specification_dir='../openapi/')
        app.app.json_encoder = JSONEncoder
        app.add_api('openapi.yaml', pythonic_params=True)
        app.app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///todoitems.db"
        db.init_app(app.app)
        with app.app.app_context():
            db.create_all()
        return app.app

    def tearDown(self):
        db.session.remove()
        db.drop_all()
