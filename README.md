## Running the server

* To start both the front and back end, run `docker-compose up -d` from the root of this project.
* This will start the server at http://localhost:8000, as well as the front end at http://localhost:3000

## Interactive documentation

* To view the API documentation run the server as above and go to http://localhost:8080/ui
* On that page you can also interact with the API endpoints to create, read, update and delete To Do Items.

## Running the tests

There is a suite of integration tests at `backend/tests`. There are various ways to run these. One is to install the command line program `tox` and then:
```
    $ cd backend
    $ tox
```
The test runner will output some deprecation warnings; these can be ignored. 

## Architecture

* The server is built using Flask, which is a simple Python MVC framework, providing its own routing and error responses.
* A basic form of persistence is provided by SQLite, which saves data to a local binary file. Hence there is no need to install a database.
* The SQLAlchemy ORM is used to simplify CRUD operations and ensure their security.
* The test runner creates its own separate SQLite instance.
